#+BEGIN_COMMENT
.. title: Drops-Theme
.. description: My personal theme for Emacs 
.. slug: Drops-Theme
.. date: 2021-07-05 11:00:00 UTC+02:00
.. tags: emacs, drops-theme
#+END_COMMENT

(deftheme DrOps
  "Clean Theme Created 2019-06-04.")

(custom-theme-set-variables
 'DrOps
 '(ansi-color-names-vector ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(org-ellipsis " ▼")
 '(org-hide-block-startup t)
 '(org-hide-emphasis-markers t)
 '(org-hide-leading-stars t)
 '(org-startup-align-all-tables t)
 '(org-startup-folded (quote content))
 '(org-startup-with-inline-images t)
 '(org-support-shift-select (quote always))
 '(ranger-override-dired (quote ranger))
 '(ranger-override-dired-mode t)

 )

(custom-theme-set-faces
 'DrOps
 '(default ((t (:inherit nil :extend nil :stipple nil :background "black" :foreground "gray90" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 128 :width normal :foundry "JB" :family "FreeSans"))))
 '(bold ((t (:weight extra-bold))))
 '(dired-directory ((t (:inherit nil :background "midnight blue" :foreground "white"))))
 '(flyspell-incorrect ((t (:inverse-video t))))
 '(hbut ((t (:background "white" :foreground "medium blue" :weight extra-bold))) t)
 '(hydra-face-blue ((t (:foreground "DodgerBlue4" :weight bold))))
 '(ledger-occur-xact-face ((t (:background "gray8"))))
 '(ledger-font-xact-cleared-face ((t (:inherit ledger-font-payee-cleared-face :foundry "JB" :family "Jetbrains Mono"))))
 '(ledger-font-xact-highlight-face ((t (:inherit ledger-occur-xact-face :extend t :foundry "JB" :family "Jetbrains Mono"))))
 '(link ((t (:underline t :weight bold))))
 '(link-visited ((t (:inherit link :weight normal))))
 '(mastodon-boosted-face ((t (:foreground "chartreuse" :underline t))))
 '(mastodon-handle-face ((t (:inherit default :height 0.5))))
 '(org-backlink-mode-face ((t (:background "brown" :foreground "white"))))
 '(org-code ((t (:family "monospaced"))))
 '(org-date ((t (:weight bold))))
 '(org-default ((t (:inherit default :family "sans serif"))))
 '(org-done ((t (:foreground "green" :weight bold))))
 '(org-ellipsis ((t (:foreground "dim gray"))))
 '(org-hide ((t (:foreground "black" :foundry "JB" :family "Jetbrains Mono"))))
 '(org-link ((t (:inherit link))))
 '(org-meta-line ((t (:inherit font-lock-comment-face :foreground "gray30"))))
 '(org-special-keyword ((t (:foreground "gray20"))))
 '(org-super-agenda-header ((t (:inherit org-agenda-structure :overline t))))
 '(org-table ((t (:foreground "LightSkyBlue" family "Jetbrains Mono" :foundry "JB"))))
 '(org-tag ((t (:foreground "gray25" :slant italic :weight bold :foundry "JB" :family "Jetbrains Mono"))))
 '(org-todo ((t (:foreground "red" :weight bold :foundry "JB" :family "Jetbrains Mono"))))
 '(outline-1 ((t (:inherit font-lock-function-name-face :family "Jetbrains Mono" :foundry "JB" :foreground "#ff3333" :weight bold :height 1.5))))
 '(outline-2 ((t (:inherit font-lock-function-name-face :family "Jetbrains Mono" :foundry "JB" :foreground "#ff9933" :height 1.3 ))))
 '(outline-3 ((t (:inherit font-lock-function-name-face :family "Jetbrains Mono" :foundry "JB" :foreground "#ffff66" :weight bold :height 1.2))))
 '(outline-4 ((t (:inherit font-lock-function-name-face :family "Jetbrains Mono" :foundry "JB" :f:oreground "#66ff66" :height 1.1))))
 '(outline-5 ((t (:inherit font-lock-function-name-face :family "Jetbrains Mono" :foundry "JB" :foreground "#9999ff" :weight bold))))
 '(outline-6 ((t (:inherit font-lock-function-name-face :family "Jetbrains Mono" :foundry "JB" :foreground "#6666ff"))))
 '(outline-7 ((t (:inherit font-lock-function-name-face :family "Jetbrains Mono" :foundry "JB" :foreground "#ff99ff"))))
 '(tab-bar ((t (:inherit variable-pitch :background "grey30" :foreground "black"))))
 '(tab-bar-tab ((t (:inherit tab-bar :background "yellow" :box (:line-width 1 :style released-button)))))
 '(tab-bar-tab-inactive ((t (:inherit tab-bar-tab :background "gray40"))))
 '(window-divider-first-pixel ((t (:foreground "gray3"))))
 '(window-divider-last-pixel ((t (:foreground "gray3"))))
 )


(provide-theme 'DrOps)
